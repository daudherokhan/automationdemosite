﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutomationDemo.Entitites
{
    public class GeneralResponse
    {
        public GeneralResponse()
        {
        }

        public bool HasError { get; set; }
        public string Error { get; set; }

        public object Data { get; set; }

    }
}