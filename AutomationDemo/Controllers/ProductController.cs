using AutomationDemo.Classes;
using AutomationDemo.Entitites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AutomationDemo.Controllers
{
    public class ProductController : ApiController
    {

        [Route("api/product/AddProduct")]
        [HttpPost]
        public GeneralResponse AddProduct(Product product)
        {
            return ProductBC.AddProduct(product);
        }


        [Route("api/product/GetProduct")]
        [HttpGet]
        public GeneralResponse GetProduct(string id)
        {
            return ProductBC.GetProduct(id);
        }

        [Route("api/product/GetProducts")]
        [HttpGet]
        public GeneralResponse GetProducts(string catalog)
        {
            return ProductBC.GetProductsForCatalog(catalog);
        }



        [Route("api/product/UpdateProduct")]
        [HttpPut]
        public GeneralResponse UpdateProduct(Product product)
        {
            return ProductBC.UpdateProduct(product);
        }

        [Route("api/product/DeleteProduct")]
        [HttpDelete]
        public GeneralResponse DeleteProduct(string id)
        {
            return ProductBC.RemoveProduct(id);
        }


        [Route("api/product/DeleteProducts")]
        [HttpDelete]
        public GeneralResponse DeleteProducts(string catalog)
        {
            return ProductBC.DeleteProducts(catalog);
        }
    }
}