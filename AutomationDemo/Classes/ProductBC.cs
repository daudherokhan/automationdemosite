using AutomationDemo.Entitites;
using Microsoft.Azure.Documents.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AutomationDemo.Classes
{
    public static class ProductBC
    {
        private const string connectionUri = "mongodb+srv://dbUser:dbPassword@testautomation.tqhunvp.mongodb.net/?retryWrites=true&w=majority";

        public static GeneralResponse AddProduct(Product product)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);
                // Send a ping to confirm a successful connection
                try
                {
                    product.Id = Guid.NewGuid().ToString();
                    client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").InsertOne(product);
                }
                catch (Exception ex)
                {
                    string error = "ERROR";
                }

                response.Data = product;
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }

            return response;
        }

        public static GeneralResponse GetProduct(string id)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);

                Product item = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").AsQueryable().Where(x => x.Id == id).FirstOrDefault();
                response.Data = item;
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }
            return response;
        }


        public static GeneralResponse GetProductsForCatalog(string catalog)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                List<Product> results = new List<Product>();

                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);

                results = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").AsQueryable().Where(x => x.Catalog == catalog).ToList();
                response.Data = results;
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }
            return response;
        }


        

        public static List<string> GetAllCatalogs()
        {
            List<string> response = new List<string>();
            try
            {
                List<Product> results = new List<Product>();

                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);
                var filter = Builders<Product>.Filter.Empty;
                List<Product> allProducts = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").Find(filter).ToList();

                if (allProducts != null && allProducts.Count() > 0)
                {
                    foreach(Product product in allProducts)
                    {
                        if(!response.Contains(product.Catalog))
                        {
                            response.Add(product.Catalog);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
              
            }
            return response;
        }

        public static GeneralResponse UpdateProduct(Product product)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);


                var filter = Builders<Product>.Filter.Eq(restaurant => restaurant.Id, product.Id);
                response.Data = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").ReplaceOne(filter, product);
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }

            return response;
        }


        public static GeneralResponse RemoveProduct(string id)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);

                var filter = Builders<Product>.Filter.Eq(r => r.Id, id);
                var results = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").DeleteOne(filter);
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }

            return response;
        }


        public static GeneralResponse DeleteProducts(string catalog)
        {
            GeneralResponse response = new GeneralResponse();
            try
            {
                var settings = MongoClientSettings.FromConnectionString(connectionUri);
                // Set the ServerApi field of the settings object to Stable API version 1
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                // Create a new client and connect to the server
                var client = new MongoClient(settings);

                var filter = Builders<Product>.Filter.Eq(r => r.Catalog, catalog);
                var results = client.GetDatabase("dbDemo").GetCollection<Product>("DemoCollection").DeleteMany(filter);
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.Error = ex.Message;
            }
            return response;
        }
    }
}